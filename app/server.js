const express = require('express');
const app = express();
const port = 3000;
const DB_USERNAME = process.env.MYSQL_USERNAME;
const DB_PASSWORD = process.env.MYSQL_PASSWORD;

app.get('/health', (req, res) => {
    res.send('Healthy!!!').status(200);
});

app.get('/', (req, res) => {
    res.json({
        DB_USERNAME: DB_USERNAME,
        DB_PASSWORD: DB_PASSWORD
    }).status(200);
})
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
